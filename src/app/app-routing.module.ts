import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { TodoComponent } from './todo/todo.component';

const routes: Routes = [
  {
    path:"",
    component:TodoComponent
  },
   //Wild Card Route for 404 request
   { 
    path: '**', 
    pathMatch: 'full', 
    component: PagenotfoundComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
