import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  list:any=[];
  listForm: FormGroup;
  isShow:boolean=true;
  completed: boolean = false;

  constructor(private fb:FormBuilder) { }

  ngOnInit(): void{
    this.listForm = this.fb.group({
      task:['',[Validators.required]]
    })
  }
  adduser(){
    this.isShow=false;
  }
  //save the task
  onSave(){
    this.listForm.markAllAsTouched();
    if(this.listForm.invalid){
      return;
    }
    else{
      this.isShow=true;
      this.list.push({id:this.list.length,name:this.listForm.value}); 
      this.listForm.reset(); 
    }
  }
  //for delete the task
  deleteTask(id){
    let index=this.list.findIndex(e=>e.id == id);
    if(index != -1){
      this.list.splice(index,1)
    }
    console.log(id)
  }
  back(){
    this.isShow=true;
  }
  //mark the task
  markDone(id){
    this.list.map((v,i)=>{
      if(i==id) v.completed=!v.completed;
      return v;
    })
  }
}
